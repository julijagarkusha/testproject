const cropString = () => {
  const initialString = "here goes very long text that shouldn't stretch this block nor by width nor by height. It should be cut " +
    "by three periods like this Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. " +
    "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies " +
    "nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, " +
    "vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis " +
    "pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, " +
    "porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus " +
    "viverra nulla ut metus varius laoreet. Quisque rutrum.";
  const cardsInfo =document.querySelectorAll(".cardItem__info");
  cardsInfo.forEach(cardInfo => {
    if(window.screen.width <= 750) {
      if (cardInfo.innerHTML.length >= 307) {
        cardInfo.innerHTML = `${initialString.substring(0, 304)}...`;
      }
    }
  });
};

export default cropString;