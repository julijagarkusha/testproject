const mobileMenuToggle = () => {
  document.querySelector("header").classList.toggle("mobileMenu--show");
};

export default mobileMenuToggle;
