import cardToggle from "./cardToggle.js";
import mobileMenuToggle from "./mobileMenuToggle.js";
import cropString from "./cropString";
import cardClickAll from "./cardClickAll";

const cards = document.querySelectorAll(".cardItem");
cards.forEach(card => {
  card.addEventListener("click", cardToggle)
});

const mobileMenuElement = document.querySelector(".menu--mobile");
mobileMenuElement.addEventListener("click", mobileMenuToggle);

cropString();

document.addEventListener("click", cardClickAll);