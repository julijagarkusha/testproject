const cardToggle = (event) => {
  const target = event.target;
  const cards = document.querySelectorAll(".cardItem");
  cards.forEach(card => {
    card.classList.remove('cardItem--active')
  });
  if (target.classList.contains("cardItem")) {
    target.classList.add("cardItem--active")
  } else if (target.parentNode.classList.contains("cardItem")) {
    target.parentNode.classList.add("cardItem--active")
  } else {
    target.parentNode.parentNode.classList.add("cardItem--active")
  }
};

export default cardToggle;
