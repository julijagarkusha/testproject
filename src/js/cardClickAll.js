const cardClickAll = (event) => {
  const target = event.target;
  const cardItems = document.querySelectorAll(".cardItem");
  if (!target.parentNode.parentNode.classList.contains("cardItem")) {
    cardItems.forEach(cardItem => {
      cardItem.classList.remove("cardItem--active")
    })
  } 
};

export default cardClickAll;
